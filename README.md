# Vimouse
Yet another project of mine which uses the rp-pico, a joystick and it's HID capabilities. This time it's a joystick that moves the cursor around in vim. Made as a joke, for nano users.

Wiring diagram:
![wiring](https://gitlab.com/slusheea/awfulmouse/-/raw/main/images/wiringdiagram.png)
This diagram is re-used from my previous joystick project, you don't actually need to connect the switch pin this time.

This time I have used a debug probe, so if you don't have one you'll have to change the runner in `.cargo/config.toml`:
```toml
# runner = "probe-run --chip RP2040" # Comment this one
# runner = "cargo embed"
runner = "elf2uf2-rs -d" # Uncomment this one
```
```toml
[target.thumbv6m-none-eabi]
runner = "probe-run --chip RP2040" # change this to "elf2uf2-rs -d"
```

I have left the debugger messages on the code, but if you want to disable them just delete the following:
```rs
use defmt::info;

// And anything with the info macro
info!("");
```

I haven't bothered to make a 3D printed case for it either this time, but here is how it looks:
![picture](https://gitlab.com/slusheea/awfulmouse/-/raw/main/images/awfulmouse.jpg)
Up is towards the USB port, and from this you can deduce the rest.
