#![no_std]
#![no_main]

use bsp::entry;
use defmt::info;
use defmt_rtt as _;
use panic_probe as _;

// USB Human Interface Device (HID) Class support
use rp_pico::hal::{pac::interrupt, usb::UsbBus};
use usb_device::{class_prelude::UsbBusAllocator, prelude::*};
use usbd_hid::{
    descriptor::{generator_prelude::SerializedDescriptor, KeyboardReport},
    hid_class::HIDClass,
};

// adds .read() to adc pins
use embedded_hal::adc::OneShot;

// Provide an alias for our BSP so we can switch targets quickly.
// Uncomment the BSP you included in Cargo.toml, the rest of the code does not need to change.
use rp_pico as bsp;
// use sparkfun_pro_micro_rp2040 as bsp;

use bsp::hal::{
    adc::Adc,
    clocks::{init_clocks_and_plls, Clock},
    pac,
    sio::Sio,
    watchdog::Watchdog,
};

static mut USB_DEVICE: Option<UsbDevice<UsbBus>> = None; // The USB Device Driver (shared with the interrupt).
static mut USB_BUS: Option<UsbBusAllocator<UsbBus>> = None; // The USB Bus Driver (shared with the interrupt).
static mut USB_HID: Option<HIDClass<UsbBus>> = None; // The USB Human Interface Device Driver (shared with the interrupt).

#[entry]
fn main() -> ! {
    let mut pac = pac::Peripherals::take().unwrap();
    let core = pac::CorePeripherals::take().unwrap();
    let mut watchdog = Watchdog::new(pac.WATCHDOG);
    let sio = Sio::new(pac.SIO);

    // External high-speed crystal on the pico board is 12Mhz
    let external_xtal_freq_hz = 12_000_000u32;
    let clocks = init_clocks_and_plls(
        external_xtal_freq_hz,
        pac.XOSC,
        pac.CLOCKS,
        pac.PLL_SYS,
        pac.PLL_USB,
        &mut pac.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();

    let usb_bus = UsbBusAllocator::new(UsbBus::new(
        pac.USBCTRL_REGS,
        pac.USBCTRL_DPRAM,
        clocks.usb_clock,
        true,
        &mut pac.RESETS,
    ));

    // Note (safety): This is safe as interrupts haven't been started yet
    unsafe {
        USB_BUS = Some(usb_bus);
    }
    let bus_ref = unsafe { USB_BUS.as_ref().unwrap() };
    let usb_hid = HIDClass::new(bus_ref, KeyboardReport::desc(), 60);
    unsafe {
        USB_HID = Some(usb_hid);
    }

    // UsbVidPid defines the vendor ID and the product ID of the USB device.
    // the device class code is documented on the usb.org website.
    let usb_dev = UsbDeviceBuilder::new(bus_ref, UsbVidPid(0x16c0, 0x27da))
        .manufacturer("Slushee")
        .product("Vimouse")
        .serial_number("1.0.0")
        .device_class(0x03)
        .build();
    unsafe {
        USB_DEVICE = Some(usb_dev);
    }

    // Enable usb interrupt
    unsafe {
        pac::NVIC::unmask(rp_pico::hal::pac::interrupt::USBCTRL_IRQ);
    }

    let mut delay = cortex_m::delay::Delay::new(core.SYST, clocks.system_clock.freq().to_Hz());

    let pins = bsp::Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    let mut adc = Adc::new(pac.ADC, &mut pac.RESETS);
    let mut analog0 = pins.gpio26.into_floating_input();
    let mut analog1 = pins.gpio28.into_floating_input();

    // 0x52   ↑
    // 0x50 ←   → 0x4F
    // 0x51   ↓
    // https://usb.org/sites/default/files/hut1_3_0.pdf

    loop {
        let mut keycodes: [u8; 6] = [0, 0, 0, 0, 0, 0];

        let x: u16 = adc.read(&mut analog1).unwrap();
        let y: u16 = adc.read(&mut analog0).unwrap();

        if x < 1000 {
            keycodes[0] = 0x50;
            info!("←");
        } else if x > 2000 {
            keycodes[0] = 0x4F;
            info!("→");
        }

        if y < 1000 {
            keycodes[1] = 0x51;
            info!("↓");
        } else if y > 2000 {
            keycodes[1] = 0x52;
            info!("↑");
        }

        let movement = KeyboardReport {
            modifier: 0,
            reserved: 0,
            leds: 0,
            keycodes,
        };

        keyboard(movement).ok().unwrap_or(0);
        delay.delay_ms(5);
    }
}

fn keyboard(report: KeyboardReport) -> Result<usize, usb_device::UsbError> {
    cortex_m::interrupt::free(|_| unsafe {
        // Now interrupts are disabled, grab the global variable and, if
        // available, send it a HID report
        USB_HID.as_mut().map(|hid| hid.push_input(&report))
    })
    .unwrap()
}

// This function is called whenever the USB Hardware generates an interrupt request.
#[allow(non_snake_case)]
#[interrupt]
unsafe fn USBCTRL_IRQ() {
    // Handle USB request
    let usb_dev = USB_DEVICE.as_mut().unwrap();
    let usb_hid = USB_HID.as_mut().unwrap();
    usb_dev.poll(&mut [usb_hid]);
}
